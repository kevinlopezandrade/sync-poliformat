# SynPoliformat

This repo contains the implementation of a graphical user interface to
synchronize the files stored at the UPV Student platform (Poliformat) to a
local folder. Java and Kotlin are required to run the application.

### UI

![App](plots/app.png)

![App](plots/sync.png)
